import { Container, interfaces } from "inversify";
import { TYPES } from "./types";
import { RestAPIRequester } from "./infrastructure/shared/Http/RestAPIRequester";
import { GetUsers } from "./application/users-list/get-users";
import { RandomUserRepository } from "./infrastructure/RandomUsersAPI/RandomUserRepository";
import { VueStateManager } from "./infrastructure/state-manager/vue-state-manager";

export class ContainerApp {
    private instance: Container;

    private constructor() {
        this.instance = new Container();
        this.instance.bind(TYPES.HttpRequestConfig).toDynamicValue(() => {
            return {
                baseURL: 'https://randomuser.me/api/'
            }
        });
        this.instance.bind(TYPES.STATE_MANAGER).to(VueStateManager);
        this.instance.bind(TYPES.RestAPIRequester).to(RestAPIRequester);
        this.instance.bind(TYPES.GetUsers).to(GetUsers);
        this.instance.bind(TYPES.UsersListRepository).to(RandomUserRepository);
    }

    static create(){
        return new ContainerApp();
    }

    resolveAPIRequester(){
        return this.instance.resolve<RestAPIRequester>(RestAPIRequester);
    }

    resolveGetUsers(){
        return this.instance.resolve<GetUsers>(GetUsers);
    }

    resolveGetUsersListRepository() {
        return this.instance.resolve<RandomUserRepository>(RandomUserRepository)
    }
}