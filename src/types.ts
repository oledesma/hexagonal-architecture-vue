export const TYPES = {
    RestAPIRequester: Symbol.for('RestAPIRequester'),
    HttpRequestConfig: Symbol.for('HttpRequestConfig'),
    GetUsers: Symbol.for('GetUsers'),
    RemoveFavoriteUser: Symbol.for('RemoveFavoriteUser'),
    SetFavoriteUser: Symbol.for('SetFavoriteUser'),
    ExportUsersCsv: Symbol.for('ExportUsersCsv'),
    UsersListRepository: Symbol.for('UsersListRepository'),
    WINDOW: Symbol.for('WINDOW'),
    STATE_MANAGER: Symbol.for('STATE_MANAGER'),
    BASE_STATE_MANAGER: Symbol.for('BASE_STATE_MANAGER'),
    APPLICATION: Symbol.for('APPLICATION'),
    VUE: Symbol.for('VUE'),
};
  