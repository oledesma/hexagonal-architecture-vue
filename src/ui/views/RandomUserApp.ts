import { ContainerApp } from "@/containerapp";
import { User } from "@/domain/user/user";
import { UsersList } from "@/domain/users-list/users-list";
import { Ref, ref } from "vue";

const app: ContainerApp = ContainerApp.create();
export const userList: Ref<UsersList> = ref({} as any)

export const useGetUserList = async () => {
    userList.value = await app.resolveGetUsers().run();
    const listNacionalidadesRepeated = userList.value.getUsersList().map((user: User) => user.getNacionalidad());
    const listNacionalidades = listNacionalidadesRepeated.filter((item, pos) => listNacionalidadesRepeated.indexOf(item) == pos)
    return {
        userList,
        listNacionalidades
    };
}

export const getUserListFiltered = (users: User[], filters: {edad: any, genero: string, nacionalidad: string}): User[] => {
    const today = new Date();

    return users.filter((user: User) => {
        const fecha_nacimiento = new Date(user.getFechaNacimiento());
        const edad = today.getFullYear() - fecha_nacimiento.getFullYear();
        
        if(edad === filters.edad && user.getGenero() === filters.genero && user.getNacionalidad() === filters.nacionalidad) {
            return user;
        }
    });
}