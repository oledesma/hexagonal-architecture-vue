export interface HttpAPIRequester {
    request<T>(request: any): Promise<any>;
}