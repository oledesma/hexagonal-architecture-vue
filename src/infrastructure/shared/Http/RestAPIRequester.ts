import "reflect-metadata";
import { injectable, inject } from 'inversify';
import { TYPES } from '@/types';
import { HttpAPIRequester } from './HttpAPIRequester';
import { HttpClient } from '../HttpClient/HttpClient';
import { HttpRequest } from '../HttpClient/interfaces/HttpRequest';
import { HttpResponse } from '../HttpClient/interfaces/HttpResponse';
import { HttpRequestConfig } from '../HttpClient/interfaces/HttpRequestConfig';

@injectable()
export class RestAPIRequester implements HttpAPIRequester {
  private client: HttpClient;

  constructor(@inject(TYPES.HttpRequestConfig) private readonly config: HttpRequestConfig) {
    this.client = new HttpClient(config);
  }

  public request = async <T>(request: HttpRequest): Promise<HttpResponse> => {
    return await this.client.request<T>(request);
  };

  public getUrl = () => {
    return this.client.getInstance().defaults.baseURL;
  };
}
