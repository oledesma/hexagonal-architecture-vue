import { AxiosRequestConfig } from 'axios';
export interface HttpRequestConfig extends AxiosRequestConfig {}
