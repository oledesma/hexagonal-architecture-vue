import { AxiosRequestHeaders } from 'axios';

export type HttpMethod =
  | 'get'
  | 'GET'
  | 'delete'
  | 'DELETE'
  | 'head'
  | 'HEAD'
  | 'options'
  | 'OPTIONS'
  | 'post'
  | 'POST'
  | 'put'
  | 'PUT'
  | 'patch'
  | 'PATCH'
  | 'purge'
  | 'PURGE'
  | 'link'
  | 'LINK'
  | 'unlink'
  | 'UNLINK';

interface APIGatewayProxyEventHeaders {
  [name: string]: string | undefined;
}

export type HttpRequestHeaders = AxiosRequestHeaders | Record<string, string> | APIGatewayProxyEventHeaders | undefined;

export interface HttpRequest {
  path: string;
  queryParameters?: any;
  headers?: HttpRequestHeaders;
  httpMethod?: any;
  data?: any;
  responseType?: any;
}
