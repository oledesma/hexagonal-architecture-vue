import { AxiosInstance } from 'axios';
export interface HttpClientInstance extends AxiosInstance {}
