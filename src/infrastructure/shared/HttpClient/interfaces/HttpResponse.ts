import { HttpRequestConfig } from './HttpRequestConfig';

export type HttpResponseHeaders = Record<string, string> & {
  'set-cookie'?: string[];
};

export interface HttpResponse {
  data?: any;
  results?: any;
  status?: number | undefined;
  statusText?: string;
  headers?: HttpResponseHeaders;
  config?: HttpRequestConfig;
  message?: string;
}
