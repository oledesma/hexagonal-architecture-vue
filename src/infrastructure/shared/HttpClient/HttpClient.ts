import axios from 'axios';
import { AxiosRequestHeaders } from 'axios';
import { HttpClientInstance } from './interfaces/HttpClientInstance';
import { HttpRequestConfig } from './interfaces/HttpRequestConfig';
import { HttpResponse } from './interfaces/HttpResponse';
import { HttpRequest } from './interfaces/HttpRequest';

export class HttpClient {
  protected readonly instance: HttpClientInstance;
  constructor(config: HttpRequestConfig) {
    this.instance = axios.create({
      baseURL: config.baseURL,
    });
    this.initializeResponseInterceptor();
  }

  public getInstance = () => {
    return this.instance;
  };

  public request = async <T>(request: HttpRequest): Promise<HttpResponse> => {
    return await this.instance.request<T>({
      url: request.path,
      headers: request.headers as AxiosRequestHeaders,
      params: request.queryParameters,
      method: request.httpMethod,
      data : request.data,
      responseType: request.responseType
    });
  };

  private initializeResponseInterceptor = () => {
    this.instance.interceptors.response.use(this.handleResponse, this.handleError);
  };

  protected handleResponse = ({ data }: HttpResponse) => data;

  protected handleError = (error: any) => Promise.reject(error);
}
