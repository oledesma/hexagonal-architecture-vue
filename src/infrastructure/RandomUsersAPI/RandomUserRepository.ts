import { TYPES } from "@/types";
import { User } from "@/domain/user/user";
import { UsersListRepository } from "@/domain/users-list/users-list-repository";
import { inject, injectable} from "inversify";
import { RestAPIRequester } from "../shared/Http/RestAPIRequester";
import { UsersList } from "@/domain/users-list/users-list";

@injectable()
export class RandomUserRepository implements UsersListRepository{
    constructor(@inject(TYPES.RestAPIRequester) private readonly requester: RestAPIRequester){}
    
    public async getUsers(): Promise<UsersList> {
        const fetchRandomUsers = await this.requester.request({
            path: 'https://randomuser.me/api/',
            headers: {
                'Content-Type': 'application/json'
            }, 
            httpMethod: 'GET',
            queryParameters: new URLSearchParams('results=100')
        });

        console.log(fetchRandomUsers.results);
        
        const users: User[] = fetchRandomUsers.results.map((user: any) => new User(
            user.gender, 
            user.name.first, 
            user.dob.age,
            user.email, 
            user.nat, 
            user.dob.date, 
            user.registered.date, 
            false,
            user.picture.medium,
            `${user.location.coordinates.latitude},${user.location.coordinates.longitude}`,
            user.location.city,
            `${user.location.street.name}, ${user.location.street.number}`,
            [user.phone, user.cell] 
        ));

        return new UsersList(1, users);
    }
} 