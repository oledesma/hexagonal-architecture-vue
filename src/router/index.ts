import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import HomeView from '../ui/views/home/HomeView.vue'
import Profile from '../ui/views/profile/Profile.vue'
import ExportToCsv from '../ui/views/export/ExportToCsv.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile,
    props: true
  },
  {
    path: '/export',
    name: 'ExportToCsv',
    component: ExportToCsv
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
