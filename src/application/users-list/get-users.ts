import { Query } from "@/domain/shared/Query";
import { UsersList } from "@/domain/users-list/users-list";
import { UsersListRepository } from "@/domain/users-list/users-list-repository";
import { TYPES } from "@/types";
import { inject, injectable } from "inversify";
import { StateManager } from "../state/state-manager";

@injectable()
export class GetUsers extends Query{
    public constructor(
        @inject(TYPES.STATE_MANAGER) private readonly stateManager: StateManager,
        @inject(TYPES.UsersListRepository) private readonly userListRespository: UsersListRepository
    ){
        super();
    }

    public async run(): Promise<UsersList> {
        this.stateManager.state.userList = await this.userListRespository.getUsers();

        return this.stateManager.state.userList;
    }
}