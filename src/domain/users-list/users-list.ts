import { User } from "../user/user";

export class UsersList {
    public constructor(private id: number, private users: User[]) {}

    public getUsersList(): User[] {
        return this.users;
    }

    public getUserListFiltered(filters: {edad: any, genero: string, nacionalidad: string}): User[] {
    
        return this.users.filter((user: User) => {
            if(user.getEdad() == filters.edad && user.getGenero() == filters.genero && user.getNacionalidad() == filters.nacionalidad) {
                return user;
            }
        });
    }
}