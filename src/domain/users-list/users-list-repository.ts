import { User } from "../user/user";
import { UsersList } from "./users-list";

export interface UsersListRepository {
    getUsers(): Promise<UsersList>;
}