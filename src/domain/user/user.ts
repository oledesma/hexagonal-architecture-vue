export class User {
    public constructor(
        private genero: string,
        private nombre: string,
        private edad: string,
        private email: string,
        private nacionalidad: string,
        private fecha_nacimiento: string,
        private fecha_registro: string,
        private favorite: boolean,
        private perfil: string,
        private geolocalizacion: string,
        private ciudad: string,
        private calle: string,
        private numeros_telefono: string[]
    ) { }

    public getGenero(): string { 
        return this.genero;
    }

    public getNombre(): string {
        return this.nombre;
    }

    public getEdad(): string {
        return this.edad;
    }

    public getEmail(): string {
        return this.email;
    }

    public getNacionalidad(): string {
        return this.nacionalidad;
    }

    public getFechaNacimiento(): string {
        return this.fecha_nacimiento;
    }

    public getFechaRegistro(): string {
        return this.fecha_registro;
    }

    public getFavorite(){
        return this.favorite;
    }

    public setFavorite(value: boolean){
        this.favorite = value;
    }

    public getPerfil(): string {
        return this.perfil;
    }

    public getGeolocalizacion() {
        return this.geolocalizacion;
    }

    public getCiudad(): string {
        return this.ciudad;
    }

    public getCalle(): string {
        return this.calle;
    }

    public getNumerosTelefono() {
        return this.numeros_telefono;
    }
}